import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//Test 1:

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#list').within(() => {
        cy.contains('Hubba bubba').should('exist');
        cy.contains('Smørbukk').should('exist');
        cy.contains('Stratos').should('exist');
        cy.contains('Hobby').should('exist');
    });
});

And(/^den skal ha riktig totalpris$/, function () {
    cy.get('#price').should('have.text', '33');
});

// Test 2:

And(/^lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#deleteItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
    cy.get('#list').within(() => {
        cy.contains('Hubba bubba').should('not.exist');
        cy.contains('Smørbukk').should('not.exist');
    });
});

// Test 3:

When(/^jeg oppdaterer kvanta for en vare$/, () => {
    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
    cy.get('#list').within(() => {
        cy.contains('Hobby').should('exist');
        cy.contains('4').should('exist');
    });
});