import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//Test 1:

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').type('Ruben VB');
    cy.get('#address').type('Trondheimsveien 1');
    cy.get('#postCode').type('1234');
    cy.get('#city').type('Trondheim');
    cy.get('#creditCardNo').type('1234567812345678');
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.contains('Din ordre er registrert.').should('exist');
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').focus().blur();
    cy.get('#address').focus().blur();
    cy.get('#postCode').focus().blur();
    cy.get('#city').focus().blur();
    cy.get('#creditCardNo').focus();
    cy.get('#creditCardNo').type('12345678');
    cy.get('#creditCardNo').blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('#fullNameError').should('contain', 'Feltet må ha en verdi');
    cy.get('#addressError').should('contain', 'Feltet må ha en verdi');
    cy.get('#postCodeError').should('contain', 'Feltet må ha en verdi');
    cy.get('#cityError').should('contain', 'Feltet må ha en verdi');
    cy.get('#creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer');
});